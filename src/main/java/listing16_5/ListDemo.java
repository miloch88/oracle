//Demostruje prostą listę JList

package listing16_5;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;

class ListDemo implements ListSelectionListener {

    JList<String> jlst;
    JLabel jlab;
    JScrollPane jscrlp;

    //Tworzymy tablicę imion
    String names[] = {"Ewa", "Janina", "Robert", "Sandra", "Joanna", "Rafał", "Bolek", "Maria",
            "Krzysio", "Andrzej", "Maciek", "Tomek"}; //Zawartość tej tablicy wyświetli komponent JList

    ListDemo(){
        //Tworzymy kontener JFrame
        JFrame jfrm = new JFrame("Lista JList");

        //Wybiera FlowLayout jako menedżera układu
        jfrm.setLayout(new FlowLayout());

        //Nadaje oknu początkowe rozmiary
        jfrm.setSize(200,160);

        //Aplikacja zakończy działanie na skutek zamnknięcia okna
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Tworzy listę JList
        jlst = new JList<>(names); //Tworzy listę

        //Określa tyrb wyboru
        jlst.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); //Zmienia tyrb wyboru na pojedyńczy

        //Opakowuje listę kontenerem JScrollPane
        jscrlp = new JScrollPane(jlst); //Opakowuje listę panelem przewujającym

        //Określa preferowany rozmiar kontenera przwijającego
        jscrlp.setPreferredSize(new Dimension(120,90));

        //Tworzymy etykietę, która będzie wyświetlać teskt zachęty lub wybrane imię
        jlab = new JLabel("Wybierz imię");

        //Dodaje obiekt nasłuchujący zdarzeń listy
        jlst.addListSelectionListener(this); //Nasłuchuje zdarzeń związanych z listą

        //Umieszcza listę i etykietę w panelu zawartości
        jfrm.add(jscrlp);
        jfrm.add(jlab);

        //Wyświetla okno
        jfrm.setVisible(true);
    }

    //Obsługuje zdarzenia listy
    @Override
    public void valueChanged(ListSelectionEvent e) {
        //Pobiera indeks elementu
        int idx = jlst.getSelectedIndex(); //Pbiera indeks elemetnu listy, który został wybrany

        //Wyświetla imię, jeśli element został wybrany
        if(idx != -1)
            jlab.setText("Aktualny wybór " + names[idx]);
        else
            jlab.setText("Wybierz imię");
    }

    public static void main(String arg[]){
        //Towrzy okno w wątku rozdziału zdarzeń
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ListDemo();
            }
        });
    }
}
