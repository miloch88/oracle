/*
Program porównujący pliki z interfejsem użytkownika wykorzystującym Swing

Ta wersja zawiera pole wyboru, którego zaznaczenie powoduje wyświetleie pozycji, na której zawatość plików różni się
 */

package appendix16_17;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SwingFC implements ActionListener {

    JTextField jtfFirst; // przechowuje nazwę pierwszego pliku
    JTextField jtfSecond; // przechowuje nazwę drugiego pliku

    JButton jbtnComp; // przycisk porównania plików

    JLabel jlabFirst, jlabSecond; // wyświetlą zachęty
    JLabel jlabResult; // wyświetla wynik i komunikatay o błędach

    JCheckBox jcbLoc; // określa czy wyświetlić informację o pozycji, na której wysąpiła różnica

    SwingFC() {
        //Tworzymy nowy kontener JFrame
        JFrame jfrm = new JFrame("Porównaywanie plików");

        //Wybiera FlowLayout jako menedżera układu
        jfrm.setLayout(new FlowLayout());

        //Nadaje oknu początkowe rozmiary
        jfrm.setSize(200, 220);

        //Aplikacja zakończy działanie na skutek zamknięcia okna
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Tworzy pola tekstowe do wprowadzania nazw plików
        jtfFirst = new JTextField(14);
        jtfSecond = new JTextField(14);

        //Wiąże tekst z tymi polami
        jtfFirst.setActionCommand("plikA");
        jtfSecond.setActionCommand("plikB");

        //Tworzy przycisk Porównaj
        JButton jbtnComp = new JButton("Porównaj");

        //Dodaje obiekt nasłuchujacy zdarzeń przycsku Porónaj
        jbtnComp.addActionListener(this);

        //Tworzy etykiety
        jlabFirst = new JLabel("Pierwszy plik: ");
        jlabSecond = new JLabel("Drugi plik: ");
        jlabResult = new JLabel("");

        //Tworzy pole wyboru
        jcbLoc = new JCheckBox("Pokaż pozycję niezgodności");

        //Umieszcza komponenty w panelu zawartości
        jfrm.add(jlabResult);
        jfrm.add(jlabFirst);
        jfrm.add(jtfFirst);
        jfrm.add(jlabSecond);
        jfrm.add(jtfSecond);
        jfrm.add(jcbLoc);
        jfrm.add(jbtnComp);

        //Wyświetla okno
        jfrm.setVisible(true);

    }

    //Poruwnuje pliki gdy użytkonk naciśnie przycisk Porównaj
    @Override
    public void actionPerformed(ActionEvent e) {
        int i = 0, j = 0;
        int count = 0;

        //Najpierw sprawda czy wprowadzone zostały nazwy obu plików
        if (jtfFirst.getText().equals("")) {
            jlabResult.setText("Brak nazwy pierwszego pliku...");
            return;
        }
        if(jtfSecond.getText().equals("")){
            jlabResult.setText("Brak nazwy drugiego pliku...");
            return;
        }

        //Porównuje pliki, używa rozszerzonej instrukcj try do zarządzania plikami.
        try(FileInputStream f1 = new FileInputStream(jtfFirst.getText());
            FileInputStream f2 = new FileInputStream(jtfSecond.getText()))
        {
            //Porównuje zawartość plików
            do{
                i = f1.read();
                j = f2.read();
                if(i != j) break;
                count++;
            }while (i != -1 && j != -1);

            if(i != j){
                if(jcbLoc.isSelected())
                    jlabResult.setText("Pliki różnią się na pozycji " + count);
                else
                    jlabResult.setText("Pliki są różne.");
            }else
                jlabResult.setText("Pliki są takie same. ");

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //Tworzy okno w wątku rozdzaiłu zdarzeń
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SwingFC();
            }
        });
    }
}
