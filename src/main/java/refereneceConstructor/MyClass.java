package refereneceConstructor;


public class MyClass {

    private String str;

    //Konstruktor ma jeden parametr
    public MyClass(String s) {
        this.str = s;
    }

    //A to jest konstrukotr domyślny

    public MyClass() {
        str = "";
    }

    public String getStr() {
        return str;
    }
}
