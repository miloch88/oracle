package refereneceConstructor;

//MyFunc jest interfejsem funkcyjnym, którego metoda zwraca referencję klasy MyClass

public interface MyFunc {
    MyClass func(String s);
}
