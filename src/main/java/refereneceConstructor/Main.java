package refereneceConstructor;

// Prezentacja referencji konstruktorów.

public class Main {
    public static void main(String[] args) {

        // Tworzy referencję konstruktora klasy MyClass.
        // Ponieważ metoda func() interfejsu MyFunc wymaga przekazania
        // jednego argumentu, zatem new odwołuje się do konstruktora
        // klasy MyClass, który ma parametr, a nie do konstruktora
        // domyślnego.

        MyFunc myClassCons = MyClass::new;

        //Tworzymy instancję klasy  MyClass przy użyciu refernecji
        //konstrukotora
        MyClass mc = myClassCons.func("Testujemy");

        //Używa uteorzonej przed chwilą instancji klasy MyClass
        System.out.println("Łańcychem w zmiennej mc jest: " + mc.getStr());
    }
}
