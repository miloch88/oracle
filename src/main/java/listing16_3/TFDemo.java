package listing16_3;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class TFDemo implements ActionListener {

    JTextField jtf;
    JButton jbtnRev;
    JLabel jlabPrompt, jlabContents;

    TFDemo() {

        // Tworzy nowy kontener JFrame.
        JFrame jfrm = new JFrame("Pole tekstowe, tutaj wprowadź swój tekst: ");

        // Wybiera FlowLayout jako menedżera układu.
        jfrm.setLayout(new FlowLayout());

        // Nadaje oknu początkowe rozmiary.
        jfrm.setSize(240, 160);

        // Aplikacja zakończy działanie na skutek zamknięcia okna.
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Tworzy pole tekstowe.
        jtf = new JTextField(15);

        // Konfiguruje tekst związany z polem tekstowym.
        jtf.setActionCommand("myTF");

        // Tworzy przycisk Odwróć.
        jbtnRev = new JButton("Odwróć");

        // Dodaje obiekty nasłuchujące zdarzeń.
        jtf.addActionListener(this);
        jbtnRev.addActionListener(this);

        // Tworzy etykiety.
        jlabPrompt = new JLabel("Wprowadź tekst: ");
        jlabContents = new JLabel("");

        // Umieszcza komponenty w panelu zawartości.
        jfrm.add(jlabPrompt);
        jfrm.add(jtf);
        jfrm.add(jbtnRev);
        jfrm.add(jlabContents);

        // Wyświetla okno.
        jfrm.setVisible(true);
    }

    // Obsługa zdarzeń ActionEvent.
    public void actionPerformed(ActionEvent ae) {

        if(ae.getActionCommand().equals("Odwróć")) {
            // Przycisk Odwróć został naciśnięty.
            String orgStr = jtf.getText();
            String resStr = "";

            // Odwraca zawartość pola tekstowego.
            for(int i=orgStr.length()-1; i >=0; i--)
                resStr += orgStr.charAt(i);

            // Umieszcza odwrócony łańcuch w polu tekstowym.
            jtf.setText(resStr);
        } else
            // Użytkownik nacisnął Enter podczas wprowadzania
            // tekstu w polu tekstowym.
            jlabContents.setText("Nacisnąłeś Enter. Tekst: " +
                    jtf.getText());
    }

    public static void main(String args[]) {
        // Tworzy okno w wątku rozdziału zdarzeń.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new TFDemo();
            }
        });
    }
}
