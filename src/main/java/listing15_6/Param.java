package listing15_6;

import java.awt.*;
import java.applet.*;

/*
<param name = author value = "Herb Schildt">
*/

public class Param extends Applet{

    String author;
    String purpose;
    int ver;

    public void start(){
        String temp;

        author = getParameter("author");
        if(author == null) author = "nie istnieje";

        purpose = getParameter("purpose");
        if(purpose == null) purpose = "nie istnieje";

        temp = getParameter("version");
        try{
            if(temp != null)
                ver= Integer.parseInt(temp);
            else
                ver = 0;
        }catch (NumberFormatException exc){
            ver = -1;
        }
    }

    public void paint(Graphics g){
        g.drawString("Zadanie: " + purpose, 10,20);
        g.drawString("Autor: " + author, 10,40);
        g.drawString("Wersja: " + ver, 10,60);
    }

}
