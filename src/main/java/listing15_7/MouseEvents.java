package listing15_7;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class MouseEvents extends Applet implements MouseListener, MouseMotionListener {

    String msg = "";
    int mouseX = 0, mouseY = 0;

    public void init() {
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mouseX = 0;
        mouseY = 10;
        msg = "Kliknięcie myszy.";
        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
        msg = "Dół";
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
        msg = "Góra";
        repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mouseX = 0;
        mouseY = 10;
        msg = "Mysz w obszarze komponentu.";
        repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouseX = 0;
        mouseY = 10;
        msg = "Mysz w opuściłą komponent.";
        repaint();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
        msg = "*";
        showStatus("Mysz przeciągana, poozyvja " + mouseX +", " + mouseY);
        repaint();

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        showStatus("Przesunięcie myszy, pozycja " + e.getX() + ", " + e.getY());
    }

    public void paint(Graphics g){
        g.drawString(msg, mouseX, mouseY);
    }
}

