package klasyAnonimowe;

public class Main {
    public static void main(String[] args) {

//       Bez klasy anonimowej
//       ButtonAction a = new ButtonA();
//       a.action();
//
//        ButtonAction b = new ButtonB();
//        b.action();

//        Z klasą anonimową
//        ButtonAction a1 = new ButtonAction() {
//            @Override
//            public void action() {
//                System.out.println("Jestem z klasy anonimowej");
//            }
//        };
//        a1.action();

//        Klasa anonimowa w ButtonC
        ButtonC c = new ButtonC();
        c.addAction(new ButtonAction() {
            @Override
            public void action() {
                System.out.println("Jestem z klasy aninimowej w klasie ButtonC");
            }
        });

    }
}

interface ButtonAction {
    void action();
}

class ButtonA implements ButtonAction{

    @Override
    public void action() {
        System.out.println("Jestem przycisk A");
    }
}

class ButtonB implements ButtonAction{

    @Override
    public void action() {
        System.out.println("Jestem przyciks B");
    }
}

class ButtonC {
    void addAction(ButtonAction c){
        c.action();
    }
}