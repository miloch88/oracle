//Prezentacja użycia metody Platform.exit()

package appendix17_10;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class JavaFXEventDemo extends Application {

    Label respone;

    public static void main(String[] args) {

        launch(args);
    }

    @Override
    public void start(Stage myStage) throws Exception {

        myStage.setTitle("Zastosowanie metody Platform.exit()");

        FlowPane rootNode = new FlowPane(10, 10);

        rootNode.setAlignment(Pos.CENTER);

        Scene myScene = new Scene(rootNode, 300, 100);

        myStage.setScene(myScene);

        respone = new Label("Naciśnij przycisk");

        Button btnRun = new Button("Uruchom");
        Button btnExit = new Button("Zakończ");

        //Obsługa zdarzenia ActionEvent przycisku Uruchom
        btnRun.setOnAction((ae) -> respone.setText("Kliknąłeś przycisk Uruchom"));

        //Obsługa zdarzenia ActionEvent przycisku Zakończ
        btnExit.setOnAction((ae) -> Platform.exit());

        rootNode.getChildren().addAll(btnRun, btnExit, respone);

        myStage.show();
    }
}
