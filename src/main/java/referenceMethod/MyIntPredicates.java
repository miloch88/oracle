package referenceMethod;

//Listing 14.9
//Ta klasa definuje trzy metody statyczne, które sprawdzają określone warunki
//związane z liczbami całkowitymi

public class MyIntPredicates {

    //Metoda statczna zwracająca true, jeśli przakazana wartość jest liczbą
    //pierwszą

     static boolean isPrime(int n) {
        if (n < 2) return false;

        for (int i = 2; i <= n / i; i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    //Metoda statyczna zwracająca wartość true, jeśli przekazana wartość
    //jest parzysta
    static boolean isEven(int n) {
        return (n % 2) == 0;
    }

    // Metoda statyczna zwracająca true, jeśli przekazana wartość
    // jest większa od zera.
    static boolean isPositive(int n) {
        return n > 0;
    }
}

