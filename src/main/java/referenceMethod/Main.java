package referenceMethod;

public class Main {
    public static void main(String[] args) {

        boolean result;

/*
        Listing 14.9 Tworenie i stosowanie referencji metod statycznych

        result = numTest(MyIntPredicates::isPrime,17);
        if(result) System.out.println("17 jest liczbą pierwszą");

        result = isPrime(13);
        if(result) System.out.println("13 jest liczbą pierwszą");
*/
/*
        Listing 14.10 Tworzenie i stosowanie referenji metod instancynych

        MyIntNum myNum = new MyIntNum(12);

        //Ta instrukcja tworzy referencje metody isFactor obiektu myNum
        //Klasa abstrakcyjna (???)
        IntPredicate ip = myNum::isFactor;

        //Teraz utworzona wcześniej referencja jest używana do wywołania
        //metody isFactor() w metodzie test()
        result = ip.test(3);
        if(result) System.out.println("3 jest czynnikim liczby " + myNum.getNum());

        MyIntNum myNum1 = new MyIntNum(12);
        result = myNum.isFactor(3);
        if(result) System.out.println("3 jest czynnikim liczby " + myNum.getNum());
*/
/*
        Listing 14.11 Zastosowanie referencji, która może się odwoływać do metody
        dowolnej instancji

        MyIntNum myIntNum = new MyIntNum(12);

        //Tworzy zmieną inp odwołującą się do metody instancyjnej isFacotr()
        //Klasa abstrakcyjna (???)
        MyIntNumPredicate inp = MyIntNum::isFactor;

        result = inp.test(myIntNum, 3);
        if(result) System.out.println("3 jest czynnikiem liczby " + myIntNum.getNum());
*/

    }
}
