package referenceMethod;

// Interfejs funkcyjnych przeznaczony dla predykatów liczbowych
// operujący na dowolnym obiekcie MyIntNum oraz wartości typu int.

public interface MyIntNumPredicate {
    boolean test(MyIntNum mv, int n);
}
