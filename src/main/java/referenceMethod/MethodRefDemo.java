package referenceMethod;


public class MethodRefDemo {

    // W tej metodzie typem pierwszego parametru jest interfejs
    // funkcyjny. Oznacza to, że można do niej przekazać referencję
    // dowolnej instancji tego interfejsu, w tym także instancji
    // utworzonej przez referencję metody.

    static boolean numTest(IntPredicate p, int v) {
        return p.test(v);
    }

}

