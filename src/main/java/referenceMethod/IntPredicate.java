package referenceMethod;

//Interfejs funkcyjny przeznaczony dla predykatów liczbowych występujących na
//wartościach całkowitych.

interface IntPredicate{
    boolean test(int n);
}
