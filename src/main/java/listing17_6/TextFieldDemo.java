//Prezentacja stosowanie pól tekstowych

package listing17_6;

import javafx.application.*;
import javafx.scene.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;


public class TextFieldDemo extends Application {

    TextField tf;
    Label response;

    public static void main(String[] args) {

        //Uruchamia aplikację JavaFX, wywołując metodę launch()
        launch(args);
    }

    //Przesłonięcie metody start()
    @Override
    public void start(Stage myStage) throws Exception {
        //Określa tytuł obszaru roboczego
        myStage.setTitle("Prezentacja pola tekstowego");

        //Jako korzeń zostae użyty panel FlowPane. W tym przypadku pionowe i poziome odstępy pomiędzy
        //umieszczonymi w nim kontrolkami będą wynosić 10.
        FlowPane rootNode = new FlowPane(10,10);

        //Wyrównuje kontrolki do środka
        rootNode.setAlignment(Pos.CENTER);

        //Tworzymy obiekt scene
        Scene myScene = new Scene(rootNode, 230, 140);

        //Dodaje obiekt scene do obiektu Stage
        myStage.setScene(myScene);

        //Tworzy etykietkę pola tekstowego
        response = new Label("Jak masz na imię");

        //Tworzy przycisk do pobierania tesktu
        Button btnGetText = new Button("Pobierz imię");

        //Tworzy pole tekstowe
        tf = new TextField();

        //Określa komunikat infomracyjny
        tf.setPromptText("Wpisz imię");

        //Używamy wyrażenia labda do obsługi zdarzeń action generowanych przez pole tekstowe.
        //Zdarzenia te są generowane, gdy użytkownik naciśnie klawszi ENTER podczas wpisywania tekstu
        // w polu. W tym przypadku obsługa zdarzenia sprowadza się do pobrania i wyświetlenia wpisanego
        //imienia.
        tf.setOnAction((ae) -> response.setText("Naciśnięto ENTER. " + "Wpisane imię to: " +
                tf.getText()));

        //Wyrażenia labda używamy także do pobrania i wyświetlenia imienia po kliknęiciu przycisku
        btnGetText.setOnAction((ae) -> response.setText("Kliknieto przycisk. " + "Wpisane imię to: " +
                tf.getText()));

        //Używa sepraratoa, aby ładniej rozmieścić elementy w układzie
        Separator separator = new Separator();
        separator.setPrefWidth(180);

        //Dodaje etykietkę i kontrolkę listy do grafu sceny
        rootNode.getChildren().addAll(tf, btnGetText, separator, response);

        //Wyświetla scenę i obdszar roboczy
        myStage.show();
    }
}
