//Demonstruje pola wyboru

package listing16_4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CBDemo implements ItemListener {

    JLabel jlabSelected;
    JLabel jlabChanged;
    JCheckBox jcbAlpha;
    JCheckBox jcbBeta;
    JCheckBox jcbGamma;

    CBDemo(){
        //Tworzy kontener JFrame
        JFrame  jfrm = new JFrame("Demonstruje pola wyboru");

        //Wybiera FlowLayout jako menedżera układu
        jfrm.setLayout(new FlowLayout());

        //Nadaje oknu początkowe wymiary
        jfrm.setSize(280, 120 );

        //Aplikacja zakończy działanie na skutek zamknięcia okna
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        jlabSelected = new JLabel("");
        jlabChanged = new JLabel("");

        //Tworzy pola wyboru`
        jcbAlpha = new JCheckBox("Alfa");
        jcbBeta = new JCheckBox("Beta");
        jcbGamma = new JCheckBox("Gamma");

        //Zdarzenia generowanie przez pola wyboru są obsługiwane wspólnie przez metodę
        // itemStaterChanged() implementowaną przez klase CSDemo
        jcbAlpha.addItemListener(this);
        jcbBeta.addItemListener(this);
        jcbGamma.addItemListener(this);

        //Umieszcza pola wyboru i etykiety w panelu zawarotści
        jfrm.add(jcbAlpha);
        jfrm.add(jcbBeta);
        jfrm.add(jcbGamma);
        jfrm.add(jlabChanged);
        jfrm.add(jlabChanged);

        //Wyświetla okno
        jfrm.setVisible(true);

    }

    //Procedura obsługi zdarzeń związanych z polami wyboru
    @Override
    public void itemStateChanged(ItemEvent e) { //Obsługuje zdarzenia związane z polami wyboru
        String str = "";

        //Pobiera referencję pola wyboru, które wygenerowało zdarzenie
        JCheckBox cb = (JCheckBox) e.getItem(); //Pobiera referencję, aby sprawdzić, które pole wyboru zmnieniło stan

        //Inforumuje, które z pól wyboru zmieniło stan
        if(cb.isSelected()) //Sprawdza, jakie zdarz`enie miało miejsce
            jlabChanged.setText(cb.getText() + " zostało zaznaczone");
        else
            jlabChanged.setText(cb.getText() + " zaznaczenie zostało usunięte");

        //Informuje, które pola są wybrane
        if(jcbAlpha.isSelected()){
            str += "Alfa ";
        }
        if(jcbBeta.isSelected()){
            str += "Beta";
        }
        if(jcbGamma.isSelected()){
            str += "Gamma";
        }

        jlabSelected.setText("Te pola są wybane " + str);
    }

    public static void main(String[] args) {
        //Tworzymy okno w wątku rozdziału zdarzeń
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new CBDemo();
            }
        });
    }
}
