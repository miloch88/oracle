import java.time.LocalTime;
import java.util.Calendar;

public class Main {
    public static void main(String[] args) {

        Calendar clock = Calendar.getInstance();
        LocalTime time = LocalTime.now();

        //Zamiana podanego dnia tygodnia na liczbę, liczby po americanos, czyli od niedzieli
        System.out.println("Niedziela to: " + Integer.toString(Calendar.SUNDAY) + " dzień tygodnia");
        System.out.println("Maj to: " + Integer.toString(Calendar.JANUARY)+1 + " miesiąc w roku");

        //Aktualny czas
        System.out.println(time);
    }
}
