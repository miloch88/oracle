/*
Przykład 16.1
Program porównujący pliki z interfejsem użytkownika wykorzystującym Swing
 */

package Przykład16_1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SwingFC implements ActionListener {

    JTextField jtfFirst; //przechowuje nazwę pierwszego pliku
    JTextField jtfSecond; //przechowuje nazwę drugiego pliku

    JButton jbtnComp; //przycisk porowniania plików

    JLabel jlabFirst, jlabSecond; //wyświetlają zachęty
    JLabel jlabResult; //wyświetla wynik i komunikaty o błędach

    public SwingFC() {

        //Tworzymy nowy kontener JFrame
        JFrame jfrm = new JFrame("Porówynywanie plików");

        //Wybiera FlowLayout jako menedżera układu
        jfrm.setLayout(new FlowLayout());

        //Nadaje oknu początkowe rozmiary
        jfrm.setSize(200, 190);

        //Aplikacja zakończy działanie na skutek zamknięcia okna
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Tworzymy pole tekstowe do wprowadzenia nazw plików
        jtfFirst = new JTextField(14);
        jtfSecond = new JTextField(14);

        //Wiąże tekst z tymi polami
        jtfFirst.setActionCommand("plikA");
        jtfSecond.setActionCommand("plikB");

        //Tworzy przycisk Porównaj
        JButton jbtnComp = new JButton("Porównaj");

        //Dodaje obiekt nsłuchujący zdarzeń przycisku Porównaj
        jbtnComp.addActionListener(this);

        //Tworzy etykiety
        jlabFirst = new JLabel("Pierwszy plik: ");
        jlabSecond = new JLabel("Drugi plik: ");
        jlabResult = new JLabel("");

        //Umieszcza kompnenty w panelu zawartości
        jfrm.add(jlabFirst);
        jfrm.add(jtfFirst);
        jfrm.add(jlabSecond);
        jfrm.add(jtfSecond);
        jfrm.add(jbtnComp);
        jfrm.add(jlabResult);

        //Wyświetla okno
        jfrm.setVisible(true);
    }

    //Porównuje pliki, gdy użytkonik naciśnie przycisk Porównaj
    @Override
    public void actionPerformed(ActionEvent e) {
        int i = 0, j = 0;

        //Najpierw sprawdza, czy wprowadzone zostały nazwy obu plików
        if (jtfFirst.getText().equals("")) {
            jlabResult.setText("Brak nazwy pierwszego pliku.");
            return;
        }
        if (jtfSecond.getText().equals("")) {
            jlabSecond.setText("Brak nazwy drugiego pliku.");
            return;
        }

        //Porównuje pliki. Używa rozszerzonej instrukcji try do zarządzania plikami
        try (FileInputStream f1 = new FileInputStream(jtfFirst.getText());
             FileInputStream f2 = new FileInputStream(jtfSecond.getText())) {

            //Porównuje zawartość plików
            do{
                i = f1.read();
                j = f2.read();
                if(i != j) break;
            }while (i != -1 && j != -1);

            if (i != j)
                jlabResult.setText("Pliki są różne.");
            else
                jlabResult.setText("Błąd dostępu do pliku.");

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //Tworzy okno w ątku rozdziału zdarzeń
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SwingFC();
            }
        });
    }
}
