package listing17_2;

import javafx.application.*;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class JavaFXLabelDemo extends Application {
    public static void main(String[] args) {
        //Uruchamia aplikację JavaFX, wywołując metodę launch()
        launch(args);
    }

    //Przesłonięcie metody start
    @Override
    public void start(Stage myStage) throws Exception {

        //Określa tytuł obszaru roboczego
        myStage.setTitle("Etykiety w aplikacjach JavaFX ");

        //Używa obiektu FlowPlane jako korzenia
        FlowPane rootNode = new FlowPane();

        //Tworzy scenę
        Scene myScene = new Scene(rootNode, 300, 200);

        //Ustawia scenę w obszarze roboczym
        myStage.setScene(myScene);

        //Tworzy etykietkę
        Label myLabel = new Label("JavaFX ma duże możlwiośći");

        //Dodaje etykietkę do grafu sceny
        rootNode.getChildren().add(myLabel);

        //Wyświetla obszar roboczy i scenę
        myStage.show();
    }
}
