package przyklad14_1;

import static przyklad14_1.LambdaArgumentDemo.changeStr;

public class Main {
    public static void main (String arg[]){

        String inStr = "Wyrażenia lambda rozszerzają możliwosći Javy";
        String outStr;

        System.out.println("Łańcuch wejściowy: "  + inStr);

        StringFunc reverse = (str) -> {
          String result = "";

            for (int i = str.length()-1; i >= 0; i--) {
                result += str.charAt(i);
            }
            return result;
        };

        outStr = changeStr(reverse, inStr);
        System.out.println("Odwrócony łańcuch znakowy: " + outStr);

        outStr = changeStr((str) -> str.replace(' ', '-'), inStr);
        System.out.println("Łańcuch z zamienionymi odstępami: " + outStr);

        outStr = changeStr((str) -> {
            String result = "";
            char ch;

            for (int i = 0; i < str.length(); i++) {
                ch = str.charAt(i);
                if(Character.isUpperCase(ch))
                    result += Character.toLowerCase(ch);
                else
                    result += Character.toUpperCase(ch);
            }
            return result;
        }, inStr);

        System.out.println("Łańcuch ze mnienioną wiekośćią liter: " + outStr
        );

    }
}
