package listing16_7;

//Prosty aplet wykorzystujący Swing

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

public class MySwingApplet extends JApplet {

    JButton jbtbUp;
    JButton jbtnDowin;

    JLabel jlab;

    //Inicjalizacja apletu
    public void init(){
        try{
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    makeGUI(); //Inicjalizacja interfesu użytkownika
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    //Ten aplet nie potrzebuje przesłaniać metod start(), stop() i destroy()

    //Inicjalizuje i konfiguruję interfejs użytkownika
    private void makeGUI(){
        //Wybiera menedżera układu komponentów
        setLayout(new FlowLayout());

        //Tworzy dwa przyciski
        jbtbUp = new JButton("Góra");
        jbtnDowin = new JButton("Dół");

        //Dodaje obiekt nasłuchujący zdarzeń przycisku Góra
        jbtbUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jlab.setText("Nacisnąłeś przycisk Góra");
            }
        });

        //Dodaje obiekt nasłuchujący zdarzeń przycisku Dół
        jbtnDowin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jlab.setText("Nacisnąłeś przycisk Dół");
            }
        });

        //Umieszczenie przycsików panelu zawartości
        add(jbtbUp);
        add(jbtnDowin);

        //Tworzy etykietkę
        jlab = new JLabel("Naciśnij przycisk");

        //Umieszcze etykietkę w panelu zawartośći
        add(jlab);
    }

}
