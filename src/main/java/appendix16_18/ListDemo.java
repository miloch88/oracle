/*
Demonstruje moźliwość wyboru wielu elementów listy JList
 */

package appendix16_18;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;

public class ListDemo implements ListSelectionListener {

    JList<String> jlist;
    JLabel jlabl;
    JScrollPane jscrlp;

    //Tworzymy tablicę imion
    String names[] = {"Ewa", "Janina", "Robert",
            "Sandra", "Joanna",  "Rafał",
            "Bolek", "Maria", "Krzysio",
            "Andrzej", "Maciej", "Tomek" };

    ListDemo(){
        //Tworzy konternr JFrame
        JFrame jfrm = new JFrame("Lista JList");

        //Wybiera FlowLayout jako menedżera układu
        jfrm.setLayout(new FlowLayout());

        //Nadaje oknu początkowe rozmiary
        jfrm.setSize(200,160);

        //Aplikacja zakończy działanie na skuterk zamknięcia okna
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Tworzy listę JList
        jlist = new JList<>(names);

        //Usunięcie poniższego wiersza powoduje możliwość wyboru wielu elementów listy JList
        //(jest to domyślne zachowanie tego kompnentu)
        //jlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        //Opakowuje listę kontenerem JScrollPane
        jscrlp = new JScrollPane(jlist);

        //Określa preferowany rozmiar kontenera przewujającego
        jscrlp.setPreferredSize(new Dimension(120,90));

        //Tworzy etykietkę, któea będzie wyświetlać tekst zachęt lub wybrane imię
        jlabl = new JLabel("Wybierz imę");

        //Dodaje słuchacza zdarzeń listy
        jlist.addListSelectionListener(this);

        //Umieszcza listę i etykietkę w panelu zawartośći
        jfrm.add(jscrlp);
        jfrm.add(jlabl);

        //Wyświetla okno
        jfrm.setVisible(true);
    }

    //Obsługuje zdarzenia listy
    @Override
    public void valueChanged(ListSelectionEvent e) {
        //Pobiera indeksty wybranych elementów
        int indices[] = jlist.getSelectedIndices();

        //Wyświetla imiona, jeśli zostały wybrane
        if(indices.length != 0){
            String who = "";

            //Tworzy łańcuch złożony z wybranych imion
            for(int i  : indices)
                who += names[i] + " ";

            jlabl.setText("Aktualny wybór: " + who);
        }
        else //W przecuwnym razie ponawia zachętę
        jlabl.setText("Wybierz imię");
    }

    public static void main(String[] args) {
        //Tworzy okno w wątku rozdziału zdarzeń
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ListDemo();
            }
        });
    }
}
