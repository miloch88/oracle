package listing17_4;// Program demonstrujący stosowanie pól wyboru.

import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.geometry.*;

public class CheckboxDemo extends Application {

    CheckBox cbSmartphone;
    CheckBox cbTablet;
    CheckBox cbNotebook;
    CheckBox cbDesktop;

    Label response;
    Label selected;

    String computers;

    public static void main(String[] args) {

        // Uruchamia aplikację JavaFX, wywołując metodę launch().
        launch(args);
    }

    // Przesłonięcie metody start().
    public void start(Stage myStage) {


        // Określa tytuł obszaru roboczego.
        myStage.setTitle("Prezentacja pól wyboru");

        // Jako korzeń zostaje użyty panel FlowPane. W tym
        // przypadku pionowe i poziome odstępy pomiędzy umieszczonymi
        // w nim kontrolkami będą wynosić 10.
        FlowPane rootNode = new FlowPane(Orientation.VERTICAL, 10, 10);

        // Wyrównuje kontrolki do środka.
        rootNode.setAlignment(Pos.CENTER);

        // Tworzy obiekt Scene.
        Scene myScene = new Scene(rootNode, 230, 200);

        // Dodaje obiekt Scene do obiektu Stage.
        myStage.setScene(myScene);

        Label heading = new Label("Jakie komputery posiadasz?");

        // Tworzy etykietę, która będzie nas informować o zmianie
        // stanu pola wyboru.
        response = new Label("");

        // Tworzy etykietę, która będzie nas informować o tym,
        // które pola są zaznaczone.
        selected = new Label("");

        // Tworzy cztery pola wyboru.
        cbSmartphone = new CheckBox("Smartfon");
        cbTablet = new CheckBox("Tablet");
        cbNotebook = new CheckBox("Notebook");
        cbDesktop = new CheckBox("Stacjonarny");

        // Obsługa zdarzeń ActionEvent pól wyboru.
        cbSmartphone.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent ae) {
                if(cbSmartphone.isSelected())
                    response.setText("Zaznaczono pole 'smartfon'.");
                else
                    response.setText("Usunięto zaznaczenie pola 'smartfon'.");

                showAll();
            }
        });

        cbTablet.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent ae) {
                if(cbTablet.isSelected())
                    response.setText("Zaznaczono pole 'tablet'.");
                else
                    response.setText("Usunięto zaznaczenie pola 'tablet'.");

                showAll();
            }
        });

        cbNotebook.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent ae) {
                if(cbNotebook.isSelected())
                    response.setText("Zaznaczono pole 'notebook'.");
                else
                    response.setText("Usunięto zaznaczenie pola 'notebook'.");

                showAll();
            }
        });

        cbDesktop.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent ae) {
                if(cbDesktop.isSelected())
                    response.setText("Zaznaczono pole 'stacjonarny'.");
                else
                    response.setText("Usunięto zaznaczenie pola 'stacjonarny'.");

                showAll();
            }
        });

        // Dodaje kontrolki do grafu sceny.
        rootNode.getChildren().addAll(heading, cbSmartphone, cbTablet,
                cbNotebook, cbDesktop, response, selected);

        // Wyświetla scenę i obszar roboczy.
        myStage.show();

        showAll();
    }

    // Aktualizuje zawartość okna aplikacji.
    void showAll() {
        computers = "";
        if(cbSmartphone.isSelected()) computers = "smartfon ";
        if(cbTablet.isSelected()) computers += "tablet ";
        if(cbNotebook.isSelected()) computers += "notebook ";
        if(cbDesktop.isSelected()) computers += "stacjonarny";

        selected.setText("Zaznaczone komputery: " + computers);
    }


}
