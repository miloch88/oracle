package przyklad15_1;

import java.applet.*;
import java.awt.*;

public class Banner extends Applet implements Runnable{

    String msg = " Java rządzi siecią! ";
    Thread t;
    boolean stopFlag;

    public void init(){
        t = null;
    }

    public void start(){
        t = new Thread(this);
        stopFlag = false;
        t.start();
    }

    @Override
    public void run() {

        for(;;){
            try{
                repaint();
                Thread.sleep(250);
                if(stopFlag)
                    break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public  void stop(){
        stopFlag = true;
        t =null;
    }

    public void paint(Graphics g){
        char ch;

        ch = msg.charAt(0);
        msg = msg.substring(1, msg.length());
        msg += ch;
        g.drawString(msg, 50, 30);
    }
}
