package nestedClasses;

public class Main {
    public static void main(String[] args) {

        /*
        Klasa anonimowa - jest to stworzenie jednej instancji klasy w miejscu
        jej użycia, nie jest instancją interfesu!
        */
        Checker<Integer> isOddAnonymous = new Checker<Integer>() {
            @Override
            public boolean check(Integer object) {
                return object % 2 != 0;
            }
        };

        System.out.println(isOddAnonymous.check(123));
        System.out.println(isOddAnonymous.check(124));
        System.out.println();

        //Wyrażenie lambda - instancja interfesu funkcyjnego

        Checker<Integer> isOddLambda = object -> object % 2 != 0;

        System.out.println(isOddLambda.check(123));
        System.out.println(isOddLambda.check(124));

        //
    }
}

