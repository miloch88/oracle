//Prezentacja kontrolki List View

package listing17_5;

import javafx.application.Application;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class ListViewDemo extends Application {

    Label response;

    public static void main(String[] args) {
        //Uruchaimia apliackę JavaFX, wywołująć metodę launch()
        launch(args);
    }

    //Przesłonięcie metody strat
    @Override
    public void start(Stage myStage) throws Exception {
        //Określa tytuł obszaru roboczego
        myStage.setTitle("Prezentacja kontrolki ListView");

        //Jako korzeń zotanie użyty panel FlowPane. W tym przypadku pionowe i poziome odstępy pomiędzy
        //umieszczonymi w nim kontrolkami będzie wynosić 10.
        FlowPane rootNodde = new FlowPane(10,10);

        //Wywrównuje kontrolki do środka
        rootNodde.setAlignment(Pos.CENTER);

        //Tworzy obiekt Scene
        Scene myScene = new Scene(rootNodde, 200, 100);

        //Dodaje obiekt Scene do obiektu Stage
        myStage.setScene(myScene);

        //Tworzy etykietkę
        response = new Label("Wybierz typ komputera");

        //Tworzy listę ObservableList, zawierającą elementy, które mają być wyświetlone w kontrolce
        //listy
        ObservableList<String> computerTypes = FXCollections.observableArrayList("Smartfon",
                "Tablet", "Notebook", "Stacjonarny");

        //Tworzy kontrlkę ListView
        ListView<String> lvComputers = new ListView<>(computerTypes);

        //Określa preferowaną wysokość i szerokość
        lvComputers.setPrefSize(100,70);

        //Pobiera mofel zaznaczania elementów listy
        MultipleSelectionModel<String> lvSelModel = lvComputers.getSelectionModel();

        //Tworzy obiekt nasłuchujący zdarzeń, który będzie odpowiadał na zmiany zaznaczenia elementów
        // listy
        lvSelModel.selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                //Wyświetla nazwę wybranego elementu z listy
                response.setText("Wybranym typem komputera jest: " + newValue);
            }
        });

        //Dodaje etykietkę i kontrolkę listy do grafu sceny
        rootNodde.getChildren().addAll(lvComputers, response);

        //Wyświetla scenę i obszar roboczy
        myStage.show();
    }
}
