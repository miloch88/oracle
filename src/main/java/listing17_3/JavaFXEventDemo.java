//Prezentacja stosowania przyciskó i obsługi zdarzeń w aplikacji JavaFX

package listing17_3;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class JavaFXEventDemo extends Application {

    Label response;

    public static void main(String[] args) {
        //Uruchamia aplikację JavaFX, wywołująe metodę launch()
        launch(args);
    }

    //Przesłonięcie metody start()
    @Override
    public void start(Stage myStage) throws Exception {

        //Określa tytuł obszaru roboczego
        myStage.setTitle("Przyciski i zdarzenia JavaFX");

        //Jako korzeń zostaje użyty w panelu FlowPane. W tym przypadku pionowe
        // i pionoew odstępy pomiędzy umieszczonymi w nim kontrolkami będą wynosić 10.
        FlowPane rootNode = new FlowPane(10, 10);

        //Wyrównuje kontrolki do środka
        rootNode.setAlignment(Pos.CENTER);

        //Tworzy obiekt Scene
        Scene myScene = new Scene(rootNode, 300, 100);

        //Dodaj obiekt Scene do obiektu Stage
        myStage.setScene(myScene);

        //Tworzy etykietkę
        response = new Label("Przycisk");

        //Tworzy dwa przyciski
        Button btnUp = new Button("Góra");
        Button btnDown = new Button("Dół");

        //Obsługa zdarzenia ActionEvnt przycisku Góra
        btnUp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                response.setText("Nacisnąłeś przycisk Góra");
            }
        });

        //Obsługa zdarzenia ActionEvent przycisku Dół
        btnDown.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                response.setText("Nacisnąłeś przycisk Dół");
            }
        });

        //Dodaje etykietkę i przyciski do gradu sceny
        rootNode.getChildren().addAll(btnUp, btnDown, response);

        //Wyświetla scenę i obszar roboczy
        myStage.show();
    }
}
