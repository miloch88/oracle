package listing17_1;

// Szkielet aplikacji JavaFX.

import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;

public class JavaFXSkel extends Application {

    public static void main(String[] args) {

        System.out.println("Uruchamiamy aplikację JavaFX.");

        // Uruchamia aplikację JavaFX, wywołując metodę launch().
        launch(args);
    }

    // Przesłonięcie metody init().
    public void init() {
        System.out.println("Wewnątrz metody init().");
    }

    // Przesłonięcie metody start().
    public void start(Stage myStage) {

        System.out.println("Wewnątrz metody start().");

        // Określa tytuł sceny.
        myStage.setTitle("Szkielet aplikacji JavaFX.");

        // Tworzy korzeń. W tym przypadku będzie to układ
        // FlowPane, choć istnieje także kilka innych.
        FlowPane rootNode = new FlowPane();

        // Tworzy scenę.
        Scene myScene = new Scene(rootNode, 300, 200);

        // Określa scenę używaną przez obiekt obszaru roboczego.
        myStage.setScene(myScene);

        // Wyświetla obszar roboczy oraz scenę.
        myStage.show();
    }

    // Przesłania metodę stop().
    public void stop() {
        System.out.println("Wewnątrz metody stop().");
    }
}

