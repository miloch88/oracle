package listing16_1;

import javax.swing.*;

public class SwingDemo {

    SwingDemo() {

        JFrame jfrm = new JFrame("Prosta aplikacja Swing"); //Tworzy kontener
        jfrm.setSize(275, 100); // Określa rozmiary ranmki
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Zamknięcie okna kończy program

        JLabel jlab = new JLabel("Swing definuje interfejs użytkownika w Javie"); //Tworzy etykietę Swing
        jfrm.add(jlab); //Umieszcza etykietę w panelu
        jfrm.setVisible(true); //Wyświetla okno (czyni je widocznym)

    }

    public static void main(String argp[]){

        //Tworzy ramkę w wątku rozdziały zdarzeń
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SwingDemo();
            }
        });
    }
}

