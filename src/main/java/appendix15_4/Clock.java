package appendix15_4;

import java.applet.*;
import java.util.*;
import java.awt.*;

public class Clock extends Applet implements Runnable {

    String msg;
    Thread t;
    Calendar clock;

    boolean stopFlag;

    public void init(){
        t= null;
        msg = "";
    }

    public void start(){
        t = new Thread(this);
        stopFlag = false;
        t.start();
    }

    @Override
    public void run() {
        for(;;){
            try{
                repaint();
                Thread.sleep(1000);
                if(stopFlag)
                    break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stop(){
        stopFlag = true;
        t=null;
    }

    public void paint(Graphics g){
        clock = Calendar.getInstance();

        msg = "Aktualny czas: " + Integer.toString(clock.get(Calendar.HOUR));
        msg = msg + ":" + Integer.toString(clock.get(Calendar.MINUTE));
        msg = msg + ":" + Integer.toString(clock.get(Calendar.SECOND));

        g.drawString(msg, 30, 30);
    }
}
