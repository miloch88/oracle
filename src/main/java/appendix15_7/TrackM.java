package appendix15_7;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class TrackM extends Applet implements MouseListener, MouseMotionListener {

    int curX = 0, curY = 0;
    int oldX = 0, oldY = 0;
    boolean draw;

    public void init() {
        addMouseListener(this);
        addMouseMotionListener(this);
        draw = false;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {

        oldX = e.getX();
        oldY = e.getY();
        draw = true;
    }

    /**
     * Invoked when a mouse button has been released on a component.
     *
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        draw = false;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        curX = e.getX();
        curY = e.getY();
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        showStatus("Moving mouse at " + e.getX() + ", " + e.getY());
    }

    public void paint(Graphics g){
        if(draw){
            g.drawLine(oldX,oldY,curX,curY);
        }
    }
}
