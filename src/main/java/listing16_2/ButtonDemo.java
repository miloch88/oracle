package listing16_2;

// Demonstruje przycisk i obługę jego zdarzeń.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class ButtonDemo implements ActionListener {

    JLabel jlab;

    ButtonDemo() {

        // Tworzy nowy kontener JFrame.
        JFrame jfrm = new JFrame("Przykład przycisku");

        // Wybiera menedżera układu FlowLayout.
        jfrm.setLayout(new FlowLayout());

        // Nadaje oknu początkowe rozmiary.
        jfrm.setSize(220, 90);

        // Aplikacja zakończy działanie na skutek zamknięcia okna.
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Tworzy dwa przyciski.
        JButton jbtnUp = new JButton("Góra");
        JButton jbtnDown = new JButton("Dół");

        // Dodaje  obiekty nasłuchujące.
        jbtnUp.addActionListener(this);
        jbtnDown.addActionListener(this);

        // Umieszcza przyciski w panelu zawartości.
        jfrm.add(jbtnUp);
        jfrm.add(jbtnDown);

        // Tworzy etykietę.
        jlab = new JLabel("Naciśnij przycisk.");

        // Dodaje etykietę do kontenera.
        jfrm.add(jlab);

        // Wyświetla okno.
        jfrm.setVisible(true);
    }

    // Obsługuje zdarzenia przycisków.
    public void actionPerformed(ActionEvent ae) {
        if(ae.getActionCommand().equals("Góra"))
            jlab.setText("Nacisnąłeś przycisk Góra.");
        else
            jlab.setText("Nacisnąłeś przycisk Dół. ");
    }

    public static void main(String args[]) {
        // Tworzy okno w wątku rozdziału zdarzeń.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new ButtonDemo();
            }
        });
    }
}
