//Prezentacja obrotu, skalowania, odbicia i zamazania

package listing17_7;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.FlowPane;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;


public class EffectsAndTransformsDemo extends Application {

    double angle = 0.0;
    double scaleFactor = 0.4;
    double blurVal = 1.0;

    //Tworzy i inicjuje efekty i transformacje
    Reflection reflection = new Reflection();
    BoxBlur blur = new BoxBlur(1.0, 1.0, 1);
    Rotate rotate = new Rotate();
    Scale scale = new Scale(scaleFactor, scaleFactor);

    //Tworzy przyciski
    Button btnRotate = new Button("Obróć");
    Button btnBlur = new Button("Zamaż");
    Button btnScale = new Button("Przeskaluj");

    Label reflect = new Label("Odbicie jest zachwycające");

    public static void main(String[] args) {

        //Uruchamia aplikacę JavaFX, wywołując metodę launch()
        launch(args);
    }

    //Przesłonięcie metody strart()
    @Override
    public void start(Stage myStage) throws Exception {

        //Określa tytuł obszaru roboczgo
        myStage.setTitle("Prezentacja efektów i transformacji");

        //Jako korzeń zostej użyty  panel FlowPane. W tym przypadku pionew i poziome odstępy pomiędzy
        //umieszczonymi w nim kontrolkami będą wynosić 10.
        FlowPane rootNode = new FlowPane(20, 20);

        //Wyrównuje kontrolki do środka
        rootNode.setAlignment(Pos.CENTER);

        //Tworzy obiekt Scene
        Scene myScene = new Scene(rootNode, 300, 120);

        //dodaje obiekt Scene do obiektu Stage
        myStage.setScene(myScene);

        //Dodaje obrót do listy transformacji przyisku "Obrót"
        btnRotate.getTransforms().add(rotate);
        btnScale.getTransforms().add(scale);

        //Dodaje odbicie do listy efektów etykiety
        reflection.setTopOpacity(0.7);
        reflection.setBottomOpacity(0.3);
        reflect.setEffect(reflection);

        //Obsługa zdarzeń przycisku "Obrót"
        btnRotate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //Każde klikniięcie przycisku powoduje powiększenie konta obrotu o 15 stopni
                angle += 15.0;

                rotate.setAngle(angle);
                rotate.setPivotX(btnRotate.getWidth() / 2);
                rotate.setPivotY(btnRotate.getHeight() / 2);
            }
        });

        // Obsługa zdarzeń przycisku "Skalowanie".
        btnScale.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent ae) {
                // Każde kliknięcie przycisku powoduje powiększenie
                // współczynnika przeskalowania.
                scaleFactor += 0.1;
                if (scaleFactor > 2.0) scaleFactor = 0.4;

                scale.setX(scaleFactor);
                scale.setY(scaleFactor);

            }
        });

        //Obsługa zdarzeń przycisku "Zamazanie"
        btnBlur.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //Każde kliknięcie przycisku powoduje zmianę stopnia zamazania
                if (blurVal == 10.0) {
                    blurVal = 1.0;
                    btnBlur.setEffect(null);
                    btnBlur.setText("Zamazanie wyłączone");
                } else {
                    blurVal++;
                    btnBlur.setEffect(blur);
                    btnBlur.setText("Zamazane włączone");
                }
                blur.setWidth(blurVal);
                blur.setHeight(blurVal);
            }
        });

        //Dodaje etkietkę i kontrolkę listy do grafu sceny
        rootNode.getChildren().addAll(btnRotate, btnScale, btnBlur, reflect);

        //Wyświetla scenę i obszar roboczy
        myStage.show();
    }
}
